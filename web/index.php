<?php

require_once __DIR__.'/../vendor/autoload.php';

$app = new Silex\Application();

$app->register(
    new \Knp\Provider\ConsoleServiceProvider(),
    array(
        'console.name' => 'MyConsoleApp',
        'console.version' => '0.1.0',
        'console.project_directory' => __DIR__ . "/.."
    )
);

$app->get('/hello', function () {
    return 'Hello!';
});

$app->run();
