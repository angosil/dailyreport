<?php
/**
 * Created by PhpStorm.
 * User: sunand
 * Date: 22/12/16
 * Time: 8:24
 */

namespace dailyreport\model;

use Goutte\Client;

class CompanyDirector extends AbstractCompanyDirector
{
    private $builder = NULL;

    /**
     * AbstractCompanyDirector constructor.
     * @param AbstractCompanyBuilder $builder_in
     */
    public function __construct(AbstractCompanyBuilder $builder_in)
    {
        $this->builder = $builder_in;
    }

    /**
     * @return mixed
     */
    function buildCompany()
    {
        $this->builder->setName('Apple Inc.');
        $this->builder->setSymbol('AAPL');
        $this->builder->setState('');
        $this->builder->setZipcode('');
    }

    /**
     * @return mixed
     */
    function getCompany()
    {
        return $this->builder->getCompany();
    }

    /**
     * fill the class company from "scraping web".
     * @param Client $client
     * @return mixed
     */
    function fillCompany(Client $client)
    {
        $crawler = $client->request('GET', "https://es.finance.yahoo.com/q?s={$this->getCompany()->getSymbol()}");
        $node = $crawler->filter('div.yfi_rt_quote_summary_rt_top')->children();
        $price = floatval(str_replace(',','.',$node->filter('span span')->eq(0)->text()));
        $this->builder->setPrice($price);
        $changePrice = floatval(str_replace(',','.',$node->filter('span span')->eq(1)->text()));
        $this->builder->setChangePrice($changePrice);
        $beta = floatval(str_replace(',','.',$crawler->filter('table#table1 tr')->eq(5)->filter('td')->text()));
        $this->builder->setBeta($beta);
    }

    /**
     * save a company report to DB.
     * @return mixed
     */
    function saveCompany()
    {
        // TODO: Implement saveCompany() method.
    }
}