<?php
/**
 * Created by PhpStorm.
 * User: sunand
 * Date: 21/12/16
 * Time: 16:11
 */

namespace dailyreport\model;

class Company
{
    private $name,$symbol,$state,$zipcode,$price,$changePrice,$beta;
    /**
     * Company constructor.
     */
    public function __construct(){

    }

    /**
     * @return string
     */
    public function showCompany(){
        return $this->name.', '.$this->symbol.', '.$this->zipcode;
    }

    /**
     * @return mixed
     */
    public function getName()
    {
        return $this->name;
    }

    /**
     * @param mixed $name
     */
    public function setName($name)
    {
        $this->name = $name;
    }

    /**
     * @return mixed
     */
    public function getSymbol()
    {
        return $this->symbol;
    }

    /**
     * @param mixed $symbol
     */
    public function setSymbol($symbol)
    {
        $this->symbol = $symbol;
    }

    /**
     * @return mixed
     */
    public function getState()
    {
        return $this->state;
    }

    /**
     * @param mixed $state
     */
    public function setState($state)
    {
        $this->state = $state;
    }

    /**
     * @return mixed
     */
    public function getZipcode()
    {
        return $this->zipcode;
    }

    /**
     * @param mixed $zipcode
     */
    public function setZipcode($zipcode)
    {
        $this->zipcode = $zipcode;
    }

    /**
     * @return mixed
     */
    public function getPrice()
    {
        return $this->price;
    }

    /**
     * @param mixed $price
     */
    public function setPrice($price)
    {
        $this->price = $price;
    }

    /**
     * @return mixed
     */
    public function getChangePrice()
    {
        return $this->changePrice;
    }

    /**
     * @param mixed $changePrice
     */
    public function setChangePrice($changePrice)
    {
        $this->changePrice = $changePrice;
    }

    /**
     * @return mixed
     */
    public function getBeta()
    {
        return $this->beta;
    }

    /**
     * @param mixed $beta
     */
    public function setBeta($beta)
    {
        $this->beta = $beta;
    }
}