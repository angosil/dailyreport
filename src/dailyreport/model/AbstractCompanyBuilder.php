<?php
/**
 * Created by PhpStorm.
 * User: sunand
 * Date: 22/12/16
 * Time: 7:48
 */

namespace dailyreport\model;


abstract class AbstractCompanyBuilder
{
    /**
     * @return mixed
     */
    abstract function getCompany();
}