<?php
/**
 * Created by PhpStorm.
 * User: sunand
 * Date: 22/12/16
 * Time: 8:05
 */

namespace dailyreport\model;


class CompanyBuilder extends AbstractCompanyBuilder
{
    private $company = NULL;

    /**
     * CompanyBuilder constructor.
     */
    function __construct() {
        $this->company = new Company();
    }

    /**
     * @param $name
     */
    public function setName($name){
        $this->company->setName($name);
    }

    /**
     * @param $symbol
     */
    public function setSymbol($symbol){
        $this->company->setSymbol($symbol);
    }

    /**
     * @param $state
     */
    public function setState($state){
        $this->company->getState($state);
    }

    /**
     * @param $zipcode
     */
    public function setZipcode($zipcode){
        $this->company->getZipcode($zipcode);
    }

    /**
     * @param $price
     */
    public function setPrice($price){
        $this->company->setPrice($price);
    }

    /**
     * @param $changePrice
     */
    public function setChangePrice($changePrice){
        $this->company->setChangePrice($changePrice);
    }

    /**
     * @param mixed $beta
     */
    public function setBeta($beta)
    {
        $this->company->setBeta($beta);
    }

    /**
     * @return mixed
     */
    function getCompany()
    {
        return $this->company;
    }
}