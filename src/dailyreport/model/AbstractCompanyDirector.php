<?php
/**
 * Created by PhpStorm.
 * User: sunand
 * Date: 22/12/16
 * Time: 7:53
 */

namespace dailyreport\model;

use Goutte\Client;

abstract class AbstractCompanyDirector
{
    /**
     * AbstractCompanyDirector constructor.
     * @param AbstractCompanyBuilder $builder_in
     */
    abstract function __construct(AbstractCompanyBuilder $builder_in);

    /**
     * build the class company from DB.
     * @return mixed
     */
    abstract function buildCompany();

    /**
     * fill the class company from "scraping web".
     * @param Client $client
     * @return mixed
     */
    abstract function fillCompany(Client $client);

    /**
     * show the company.
     * @return mixed
     */
    abstract function getCompany();

    /**
     * save a company report to DB.
     * @return mixed
     */
    abstract function saveCompany();
}