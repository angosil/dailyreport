<?php
/**
 * Created by PhpStorm.
 * User: sunand
 * Date: 21/12/16
 * Time: 14:31
 */

namespace dailyreport\command;

use dailyreport\model\CompanyBuilder;
use dailyreport\model\CompanyDirector;
use Knp\Command\Command;
use Symfony\Component\Console\Input\InputInterface;
use Symfony\Component\Console\Output\OutputInterface;
use Goutte\Client;

class myCommand extends Command
{
    /**
     *
     */
    protected function configure() {
        $this
        ->setName("report")
        ->setDescription("A test command!");
    }

    /**
     * @param InputInterface $input
     * @param OutputInterface $output
     * @return int|null|void "nothings"
     */
    protected function execute(InputInterface $input, OutputInterface $output) {

        $companyBuilder = new CompanyBuilder();
        $companyDirector = new CompanyDirector($companyBuilder);
        //from bd, get data to built companies.
        $companyDirector->buildCompany();

        //from web client, get data to fill de companies.
        $client = new Client(); // Goutte Client
        $companyDirector->fillCompany($client);

        $company = $companyDirector->getCompany();

        $output->writeln($company->showCompany());
        $output->writeln('Value: '.$company->getPrice());
        $output->writeln('Change price: '.$company->getChangePrice());
        $output->writeln('Beta: '.$company->getBeta());
        $output->writeln("It works!");
    }
}