<?php

use dailyreport\command\myCommand;

set_time_limit(0);

require_once __DIR__.'/../vendor/autoload.php';

$app = new Silex\Application();

$app->register(
    new \Knp\Provider\ConsoleServiceProvider(),
    array(
        'console.name' => 'MyConsoleApp',
        'console.version' => '0.1.0',
        'console.project_directory' => __DIR__ . "/.."
    )
);

//$app = require_once dirname(__DIR__) ."/bootstrap.php";

$app->register(new \Silex\Provider\DoctrineServiceProvider(), array(
    'doctrine.odm.mongodb.connection_options' => array(
        'database' => 'my_database_name',
        'host'     => 'localhost',
    )
));

$console = &$app["console"];
$console->add(new myCommand());
$console->run();